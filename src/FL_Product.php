<?php

/**
 * FL_Product
 * Create a custom product object for simple use
 *
 * @package      FL_Product
 *
 * @author       Jurgen Oldenburg

 * @version      v0.1-alpha
 * @since        v0.1-alpha
 */


// namespace FeatherLight;


/**
 * Core functions Class
 */
class FL_Product {

    public string     $version = 'v0.1-alpha';
    public int        $id = 0;
    public string     $title = '';
    public string     $name = '';
    public string     $sort_title = '';
    public string     $sku = '';
    public string     $image = '';
    public array      $forces = [];
    public string     $max_force = '';
    public string     $regular_price = '';
    public string     $sale_price = '';
    public string     $size = '';
    public string     $in_stock = '';
    public string     $in_stock_message = '';
    public string     $screw_diameter = '';
    public string     $screw_length = '';
    public string     $elongation_length = '';
    public string     $total_length = '';
    public string     $material = '';
    public bool       $is_mountingpart = false;
    public bool       $is_gasspring = false;
    public bool       $is_replacement = false;
    public array      $meta = [];
    // public WC_Product $product_data;

    public function __construct(WC_Product $product) {
        $this->set_id($product);
        $this->set_meta($product);
        $this->set_title($product);
        $this->set_name();
        $this->set_sort_title();
        $this->set_sku($product);
        $this->set_image($product);
        $this->set_forces($product);
        $this->set_max_force();
        $this->set_regular_price($product);
        $this->set_sale_price($product);
        $this->set_in_stock($product);
        $this->set_in_stock_message($product);
        $this->set_size();
        $this->set_screw_diameter();
        $this->set_screw_length();
        $this->set_elongation_length();
        $this->set_total_length();
        $this->set_material();
        $this->set_is_mountingpart();
        $this->set_is_gasspring();
        $this->set_is_replacement();

        // $this->set_product_data($product);
    }


    /**
     * @param  WC_Product  $product
     */
    public function set_id(WC_Product $product): void {
        $this->id = $product->get_id();
    }


    /**
     * @return int
     */
    public function get_id(): int {
        return $this->id;
    }


    /**
     * Get a single custom field, or get all custom fields
     *
     * @param  string  $key  The custom field key
     *
     * @return string|boolean|array string if single item is found
     *                              false if it's not found
     *                              array if no `$key` param was passed
     */
    public function get_meta(string $key = '') {
        if (!empty($key)) {
            if(isset($this->meta[$key])) {
                return $this->meta[$key];
            }
            return false;
        }
        return $this->meta;
    }


    /**
     * Populate the local $product_meta property with all the custom fields
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_meta(WC_Product $product): void {
        $all_fields = get_post_custom($product->get_id());
        $custom_fields = [];
        foreach ($all_fields as $field_key => $field_val) {
            if (substr($field_key, 0, 1 ) !== "_") {
                $custom_fields[$field_key] = $field_val[0];
            }
        }
        $this->meta = $custom_fields;
    }


    /**
     * Set local `product_is_gasspring` property if it's a replacement gasspring
     * Determine if the product is a gasspring from the local product meta
     */
    public function set_is_gasspring(): void {
        $slag = $this->get_meta('slag');
        $type = $this->get_meta('type');
        // $gastrekveer = ($this->get_product_meta('type') == "gastrekveer");
        if(
            !empty($slag)
            && !$type
            || $type == "gastrekveer"
        ) {
            $return = true;
        } else {
            $return = false;
        }

        $this->is_gasspring = $return;
    }


    /**
     * Set local `product_is_replacement` property if it's a replacement gasspring
     * Determine if the product is a replacement gasspring from the local product meta
     */
    public function set_is_replacement(): void {
        if( $this->get_meta('type') === "vervanginginfilter" ) {
            $return = true;
        } else {
            $return = false;
        }

        $this->is_replacement = $return;
    }


    /**
     * Determine if the product is a mounting part from the local product meta
     * Set local `product_is_mountingpart` property if it's a mounting part
     */
    public function set_is_mountingpart(): void {
        if( $this->get_meta("hahntype") ) {
            $return = true;
        } else {
            $return = false;
        }

        $this->is_mountingpart = $return;
    }


    /**
     * Set local `product_data` property with the complete the WooCommerce product object
     * This is mainly for debugging purposes
     *
     * @param  WC_Product  $product  The WooCommerce product object
     */
    public function set_product_data(WC_Product $product): void {
        $this->product_data = $product;
    }


    /**
     * Get all the data of the product WooCommerce WC_Product object
     *
     * @return WC_Product
     */
    public function get_product_data(): WC_Product {
        return $this->product_data;
    }


    /**
     * Get local `product_title` property
     *
     * @return string
     */
    public function get_title(): string {
        return $this->title;
    }


    /**
     * Set local `product_title` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_title(WC_Product $product): void {
        $this->title = $product->get_title();
    }


    /**
     * @return string
     */
    public function get_name(): string {
        return $this->name;
    }


    /**
     */
    public function set_name(): void {
        $this->name = $this->get_meta('name');
    }


    /**
     * @return string
     */
    public function get_sort_title(): string {
        return $this->sort_title;
    }


    /**
     */
    public function set_sort_title(): void {

        if(!empty($this->get_name())) {
            $title_parts = explode('-', $this->get_name());

            if ($title_parts[0] < 10) {
                $title_parts[0] = '0' . $title_parts[0];
            }

            $this->sort_title = implode('-', $title_parts);
        }

    }


    /**
     * Get local `product_sku` property
     *
     * @return string
     */
    public function get_sku(): string {
        return $this->sku;
    }


    /**
     * Set local `product_sku` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_sku(WC_Product $product): void {
        $this->sku = $product->get_sku();
    }


    /**
     * Get local `product_image` property
     *
     * @return string
     */
    public function get_image(): string {
        return $this->image;
    }


    /**
     * Set local `product_image` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_image(WC_Product $product): void {
        $this->image = $product->get_image('thumbnail');
    }


    /**
     * Get local `product_forces` property
     *
     * @return array
     */
    public function get_forces(): array {
        return $this->forces['kracht-uit'];
    }


    /**
     * Set local `product_forces` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_forces(WC_Product $product): void {
        if (isset($product->get_attributes('kracht-uit')['kracht-uit'])) {
            $items = $product->get_attributes('kracht-uit')['kracht-uit']->get_data()['options'];
            // Guarantee that the order is correct
            sort($items, SORT_NUMERIC);
            $this->forces = $items;
        }
    }


    /**
     * Set max force from the forces array, or else from the custom field
     */
    public function set_max_force(): void {
        $forces = $this->forces;
        if (!empty($forces)) {
            // Get the last item from the forces array
            $this->max_force = preg_replace("/[^0-9]/", "", end($forces) );
        } elseif($this->get_meta('maxforceallowed')) {
            // Get it from custom field maxforceallowed
            $this->max_force = $this->get_meta('maxforceallowed');
        } else {
            // Get it from custom field krachtmax
            $this->max_force = $this->get_meta('krachtmax');
        }
    }


    /**
     * @return string
     */
    public function get_max_force(): string {
        return $this->max_force;
    }


    /**
     * Get local `product_regular_price` property
     *
     * @return string
     */
    public function get_regular_price(): string {
        return $this->regular_price;
    }


    /**
     * Set the local `product_regular_price` property from the $this->get_product_data() object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_regular_price(WC_Product $product): void {
        $this->regular_price = $product->get_regular_price();
    }


    /**
     * Get local `product_sale_price` property
     *
     * @return string
     */
    public function get_sale_price(): string {
        return $this->sale_price;
    }


    /**
     * Set local product_sale_price` with the sales price of the local `$this->get_product_data()` data
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_sale_price(WC_Product $product): void {
        $this->sale_price = $product->get_sale_price();
    }


    /**
     * Get local `product_size` property
     *
     * @return string
     */
    public function get_size(): string {
        return $this->size;
    }


    /**
     * Set local `product_size` property from the WooCommerce product object
     */
    public function set_size(): void {
        $this->size = $this->get_meta('lengte');
    }


    /**
     * @return string
     */
    public function get_screw_diameter(): string {
        return $this->screw_diameter;
    }


    /**
     */
    public function set_screw_diameter(): void {
        $this->screw_diameter = $this->get_meta('dschroefdraad');
    }


    /**
     * @return string
     */
    public function get_screw_length(): string {
        return $this->screw_length;
    }


    /**
     */
    public function set_screw_length(): void {
        $this->screw_length = $this->get_meta('lschroefdraad');
    }


    /**
     * @return string
     */
    public function get_elongation_length(): string {
        return $this->elongation_length;
    }


    /**
     */
    public function set_elongation_length(): void {
        $this->elongation_length = $this->get_meta('elongationlength');
    }


    /**
     * @return string
     */
    public function get_total_length(): string {
        return $this->total_length;
    }


    /**
     */
    public function set_total_length(): void {
        $this->total_length = $this->get_meta('totallength');
    }


    /**
     * Get local `product_in_stock` property
     *
     * @return string
     */
    public function get_in_stock(): string {
        return $this->in_stock;
    }


    /**
     * Set local `product_in_stock` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_in_stock(WC_Product $product): void {
        if (!empty($product->get_purchase_note())) {
            $this->in_stock = 'false';
        } else {
            $this->in_stock = 'true';
        }
    }


    /**
     * Get local `product_in_stock_message` property
     * Get local `product_in_stock_message` property
     *
     * @return string
     */
    public function get_in_stock_message(): string {
        return $this->in_stock_message;
    }


    /**
     * Set local `product_in_stock_message` property from the WooCommerce product object
     *
     * @param  WC_Product  $product  WooCommerce WC_Product object
     */
    public function set_in_stock_message(WC_Product $product): void {
        if ($this->get_in_stock() != 'true') {
            $this->in_stock_message = $product->get_purchase_note();
        }
    }


    /**
     */
    public function set_material(): void {
        $this->material = $this->get_meta('material');
    }


    /**
     * @return string
     */
    public function get_material(): string {
        return $this->material;
    }


    /**
     * Get local `product_is_mountingpart` property
     *
     * @return bool
     */
    public function is_mountingpart(): bool {
        return $this->is_mountingpart;
    }


    /**
     * Get local `product_is_gasspring` property
     *
     * @return bool
     */
    public function is_gasspring(): bool {
        return $this->is_gasspring;
    }


    /**
     * Get local `product_is_replacement` property
     *
     * @return bool
     */
    public function is_replacement(): bool {
        return $this->is_replacement;
    }



}
